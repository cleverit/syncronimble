<?php
/*
Update the following in the code below with your own settings:
<syncro_subdomain>
<syncro_api_token>
<mysql_database_host>
<mysql_database_username>
<mysql_database_password>
<mysql_database_name>
*/

$syncro_customers_json = json_decode(get("https://<syncro_subdomain>.syncromsp.com/api/v1/customers?api_key=<syncro_api_key>"), true);

add_to_db("syncro",$syncro_customers_json["customers"]);
//print_r($syncro_customers_json["customers"]);

function get($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_ENCODING , "");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept-language: en-US,en;q=0.9,ar-EG;q=0.8,ar;q=0.7'));
    $source = curl_exec($ch);
    curl_close($ch);
    return $source;
}

function add_to_db($table_name,$array_all) {

	$servername = "<mysql_database_host>";
	$username = "<mysql_database_username>";
	$password = "<mysql_database_password>";
	$dbname = "<mysql_database_name>";
	
	$allowed  = ['id','firstname','lastname','fullname','business_name','email','phone','mobile','created_at','updated_at','pdf_url','address','address_2','city','state','zip','latitude','longitude','notes','get_sms','opt_out','disabled','no_email','location_name','location_id'];
	
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
	foreach ($array_all as $array) {
		$filtered = array_filter($array,
		function ($key) use ($allowed) {
		    return in_array($key, $allowed);
		},
		ARRAY_FILTER_USE_KEY
		);

		$columns = implode(", ",array_keys($filtered));
		$columns_value = ":" . implode(",:",array_keys($filtered));
		
		$stat=$conn->prepare("INSERT INTO `$table_name`($columns)
		VALUES ($columns_value)");
		
		foreach ($filtered as $key => $val) {
		    if($val == '') $filtered[$key] = NULL;
		    if($key == "created_at" || $key == "updated_at")
		        $filtered[$key] = date('Y-m-d H:i:s', strtotime($val));
		}
		
		$stat->execute($filtered);
		
    }

}