This is provided as is with instructions on where to change things and database creation instructions within.

It's free, but if you feel the desire to send me some money to cover the initial development effort, cool. Use this link https://www.paypal.me/moeztharani.

Some important notes:

1. Uses the Nimble API and Syncro API to fetch a list of Companies (Nimble) and Customers (Syncro) and matches them.
2. For any matches, it then uses the Nimble API to pull a list of Contacts associated with the matching Company.
3. Using email address as a unique field, it will either update an existing Contact in Syncro with the information in Nimble, or create a new contact in Syncro with the information in Nimble. 
4. The script will grab and update Name, Address1, Address2, City, State, Zip, Email, Phone, Mobile and the most recently updated Description. Two important points here. One, it will grab the most recently updated phone number of Mobile type and the other phone number will be any other most recently updated phone number. So if you have more than 2 phone numbers, well, don't. :) The other point is regarding description. Nimble allows you to have multiple descriptions. This script will just take the most recently updated one and added it to the notes in Syncro.
5. Any contacts that do not have a matching company in Syncro will be ignored. We did this because I'll have contacts in Nimble (like vendors) that I don't want in Syncro, so creating a customer automatically is a bad idea for us.
6. When you need to sync, either run /nimble.php from a browser or schedule it via cron. There is some basic information provided in the browser response.
7. The script does not support deleting of contacts or syncing contacts from Syncro back to Nimble. That means that if you need to delete a contact, you need to delete it in both Nimble and Syncro.

This script does not support deleting of contacts or syncing contacts from Syncro to Nimble. That means if you need to delete a contact, you need to delete it in both Nimble and Syncro.