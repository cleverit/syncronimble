<?php
/*
Update the following in the code below with your own settings:
<nimble_token>
<syncro_api_token>
<syncro_subdomain>
<mysql_database_host>
<mysql_database_username>
<mysql_database_password>
<mysql_database_name>
*/

// CONST START
$nimble_token = "<nimble_token>";
$syncro_token = "<syncro_api_token>";

$nimble_api = "https://api.nimble.com/api/v1";
$syncro_api = "https://<syncro_subdomain>.syncromsp.com/api/v1";

$servername = "<mysql_database_host>";
$username = "<mysql_database_username>";
$password = "<mysql_database_password>";
$dbname = "<mysql_database_name>";

// CONST END


//SQL TO BE RUN WHILE SETTING UP A NEW MYSQL DATABASE:
/*
CREATE TABLE `last_updated` (
 `datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT;


CREATE TABLE `sync` (
 `nimble_id` varchar(50) NOT NULL,
 `syncro_id` varchar(50) NOT NULL,
 UNIQUE KEY `unique_nimble` (`nimble_id`),
 UNIQUE KEY `unique_syncro` (`syncro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
*/


// Start Code
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

echo "Last Updated: " . get_last_updated() . "\n<br/>";
$last_update = urlencode(get_last_updated());

$nimble_customers_json = json_decode(get("$nimble_api/contacts?access_token=$nimble_token&query=%7B%22updated%22%3A%7B%22range%22%3A%7B%22start_date%22%3A%22$last_update%22%2C%22end_date%22%3A%222050-02-23%22%7D%7D%7D&per_page=1000"), true);

echo "Start\n<br/>";
if(isset($nimble_customers_json["resources"])) {

	foreach ($nimble_customers_json["resources"] as $customers) {
		if($customers["record_type"] == "company") continue;
		
		
		
		$nimble_id = $customers["id"];
		$syncro = array();
		$nimble_customers = $customers["fields"];

		if(isset($nimble_customers["first name"])) {
			$syncro["firstname"] = $nimble_customers["first name"]["0"]["value"];
		}
		
		if(isset($nimble_customers["last name"])) {
			$syncro["lastname"] = $nimble_customers["last name"]["0"]["value"];
		}
		
		$syncro["name"] = (isset($syncro["firstname"]) ? $syncro["firstname"] : NULL) . (isset($syncro["lastname"]) ? " " . $syncro["lastname"] : "");
		
		if(isset($nimble_customers["parent company"])) {
			$syncro["business_name"] = $nimble_customers["parent company"]["0"]["value"];
		}
		
		if(isset($nimble_customers["email"])) {
			$syncro["email"] = $nimble_customers["email"]["0"]["value"];
		}
		
		if(isset($nimble_customers["description"])) {
			$syncro["notes"] = $nimble_customers["description"]["0"]["value"];
		}
		
		if(isset($nimble_customers["address"])) {
			$address_array = json_decode($nimble_customers["address"]["0"]["value"],true);
			if(isset($address_array["city"])) $syncro["city"] = $address_array["city"];
			if(isset($address_array["country"])) $syncro["country"] = $address_array["country"];
			if(isset($address_array["state"])) $syncro["state"] = $address_array["state"];
			if(isset($address_array["street"])) $syncro["address1"] = $address_array["street"];
			if(isset($address_array["zip"])) $syncro["zip"] = $address_array["zip"];
		}
		
/*		if(isset($nimble_customers["discovered related address"])) {
			$address_array = json_decode($nimble_customers["discovered related address"]["0"]["value"],true);
			if(isset($address_array["city"])) $syncro["city"] = $address_array["city"];
			if(isset($address_array["country"])) $syncro["country"] = $address_array["country"];
			if(isset($address_array["state"])) $syncro["state"] = $address_array["state"];
			if(isset($address_array["street"])) $syncro["address_1"] = $address_array["street"];
			if(isset($address_array["zip"])) $syncro["zip"] = $address_array["zip"];
		}
*/		
		if(isset($nimble_customers["phone"])) {
			foreach($nimble_customers["phone"] as $itr) {
				if($itr["modifier"] == "mobile") $syncro["mobile"] = $itr["value"];
				else $syncro["phone"] = $itr["value"];
			}
		}
		
		if($syncro_id = get_from_db($nimble_id)) {
			$response = post("$syncro_api/contacts/$syncro_id?api_key=$syncro_token",$syncro, 1);
			
			echo "updated $syncro_id $nimble_id \n<br/>";
		} else {
		    
		    if(!isset($syncro["business_name"]) || $syncro["business_name"] == '') continue;
		    
		    $business_name = urlencode($syncro["business_name"]);
		    $syncro_res = json_decode(get("$syncro_api/customers?business_name=$business_name&api_key=$syncro_token"), true);
		    
		    $syncro_cus = $syncro_res["customers"];
		    
		    if($syncro_cus && count($syncro_cus) > 0) {
		        $syncro["customer_id"] = $syncro_cus[0]["id"];
		        
		        $response_json = post("$syncro_api/contacts?api_key=$syncro_token",$syncro);
			    $response = json_decode($response_json, true);

			    if(isset($response["id"])) {
    				$syncro_id = $response["id"];
    				add_to_db($syncro_id,$nimble_id);
			
			        echo "added $syncro_id $nimble_id \n<br/>";
    			} else {
    			    echo "Couldn't add contact Syncro_Customer: $syncro_id Nimble Contact: $nimble_id \n<br/>";
    				continue;
    			}
    			
		    } else { // No Customer with matching Business Name
		        echo "Couldn't find matching Business Name in Syncro Nimble_Contact: $nimble_id \n<br/>";
		        continue;
		    }
			
		}
	}
	update_last_updated();
	echo "End\n<br/>";

}


function get($url) {
    $ch = curl_init();
    $headers = array(
        'Content-Type: application/json',
        'accept: application/json'
    );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_ENCODING , "");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $source = curl_exec($ch);
    curl_close($ch);
    return $source;
}

function post($url, $fields, $PUT = false) {
	$fields_string = json_encode($fields);
    $headers = array(
        'Content-Type: application/json',
        'accept: application/json'
    );
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, $url);
	if($PUT) curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	else curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
	return curl_exec($ch);
}

function get_from_db($nimble_id) {
	global $conn;
    $sql = 'SELECT syncro_id,
                    nimble_id
               FROM sync
              where nimble_id = ?';
 	$q = $conn->prepare($sql);
    $q->execute(["$nimble_id"]);
    $q->setFetchMode(PDO::FETCH_ASSOC);
    if($r = $q->fetch()) return $r["syncro_id"];
    else return false;
}

function get_last_updated() {
	global $conn;
    $sql = 'SELECT DATE_FORMAT(DATE_SUB(datetime, INTERVAL 7 HOUR),"%Y-%m-%dT%H:%i:%S") as datetime
               FROM last_updated
              order by datetime desc limit 1';
 
 	$q = $conn->prepare($sql);
    $q->execute();
    $q->setFetchMode(PDO::FETCH_ASSOC);
    if($r = $q->fetch()) return $r["datetime"];
    else return "2000-01-01T00:00:00";
}

function update_last_updated() {
	global $conn;
    $sql = 'INSERT INTO `last_updated` (`datetime`) VALUES (current_timestamp());';
 	$q = $conn->prepare($sql);
    $q->execute();
    return true;
}


function add_to_db($syncro_id,$nimble_id) {
	global $conn;
	$stat=$conn->prepare("INSERT INTO `sync`(`syncro_id`,`nimble_id`)
	VALUES (:syncro_id,:nimble_id)");
	$stat->bindParam(":syncro_id", $syncro_id);
	$stat->bindParam(":nimble_id", $nimble_id);
	$stat->execute();
}
